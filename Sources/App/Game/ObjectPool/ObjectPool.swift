//
//  ObjectPool.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public class ObjectPool<T: Poolable>{
    private var objects: [T]
    
    init(initializer: () -> T, count: Int){
        self.objects = []
        for _ in 0..<count{
            self.objects.append(initializer())
        }
    }
    
    func instantiate() -> T{
        let obj = objects.removeLast()
        obj.objectWillBeInstantiatedFromPool()
        return obj
    }
    
    func recycle(_ object: T){
        object.objectWillReturnToPool()
        self.objects.append(object)
    }
    
}
