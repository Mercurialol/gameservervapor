//
//  Poolable.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public protocol Poolable{
    func objectWillBeInstantiatedFromPool()
    func objectWillReturnToPool()
}

public extension Poolable{
    func objectWillBeInstantiatedFromPool(){}
    func objectWillReturnToPool(){}
}
