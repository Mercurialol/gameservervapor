//
//  Entity.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public typealias EntityID = Int

public class Entity{
    var components: [Component]
    public var id: EntityID
    
    init(id: EntityID) {
        self.components = Array<Component>(repeating: VoidComponent(), count: 32)
        self.id = id
    }
    
    public func has(componentType: ComponentID) -> Bool{
        return self.components[componentType].id != Constants.Component.TYPE_VOID
    }
    
    public func get<T>(componentType: ComponentID, type: T.Type) -> T? where T: Component{
        return self.components[componentType] as? T
    }
    
    public func addComponent(component: Component){
        self.components[component.id] = component
        component.entity = self
    }
    
    public func toJSON() -> [String: Any]{
        var dict: [String: Any] = [:]
        dict["id"] = self.id
        
        for comp in self.components.filter({ (component) -> Bool in !(component is VoidComponent) }){
            let json = comp.toJSON()
            if let key = json.0{
                dict[key] = json.1
            }
        }
        
        return dict
    }
}
