//
//  Projectile.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/28/18.
//

import Foundation

public class Projectile: Entity{

    public private(set) var parent: Entity!
    
    var directional: Bool = false
    
    public static func instantiate() -> Projectile{
        return Projectile(id: RandomIDGenerator().generate())
    }
    
    override init(id: EntityID) {
        super.init(id: id)
        self.addComponent(component: MoveComponent())
        self.addComponent(component: PositionComponent())
        self.addComponent(component: SpeedComponent())
        self.addComponent(component: ProjectileComponent())
    }
    
    @discardableResult
    func setup(parentEntity: Entity, speed: Double, meshID: MeshID) -> Projectile{
        self.parent = parentEntity
        self.setTeam(team: self.parent.getTeam()!)
        self.setSpeed(speed: speed)
        self.addComponent(component: MeshComponent(meshID: meshID))
        return self
    }
    
    @discardableResult
    func setDirectional(position: Vector3, direction: Vector3, range: Double) -> Projectile{
        if let mc = self.moveComponent(){
            let pointStopCondition = PointStopCondition.instantiate()
            
            pointStopCondition.target = Movement.move(currentPosition: position, direction: direction, step: range)
            mc.stopCondition = pointStopCondition
            mc.direction = direction

            self.directional = true
        }
        return self
    }
    
    @discardableResult
    func setTarget(target: Entity) -> Projectile{
        if let mc = self.moveComponent(){
            let stopCondition = FollowTargetStopCondition.instantiate()
            let gameObjectPositionProvider = GameObjectPositionProvider.instantiate()
            gameObjectPositionProvider.entity = target
            stopCondition.positionProvider = gameObjectPositionProvider
            mc.stopCondition = stopCondition
            
            self.directional = false
        }
        return self
    }
}
