//
//  RandomIDGenerator.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class RandomIDGenerator: IDGenerator {
    
    public func generate() -> EntityID {    
        return Int.random() % 10000
    }
}
