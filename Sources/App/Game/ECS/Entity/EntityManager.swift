//
//  EntityManager.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class EntityManager {

    private var entitiesToAdd: [Entity]
    private var entitiesToRemove: [Entity]
    
    private(set) var entities: [EntityID: Entity]
    private let idGenerator: IDGenerator
    
    var eventDidAddEntity: ((_ entity: Entity) -> Void)
    var eventDidRemoveEntity: ((_ entity: Entity) -> Void)
    
    init(){
        entities = [:]
        self.entitiesToRemove = []
        self.entitiesToAdd = []
        self.idGenerator = RandomIDGenerator()
        self.eventDidAddEntity = { _ in }
        self.eventDidRemoveEntity = { _ in }
    }
    
    public func entityWithID(id: EntityID) -> Entity?{
        return self.entities[id]
    }
    
    public func update(deltaTime: TimeInterval){
        self.removeQueuedEntities()
        self.addQueuedEntities()
    }
    
    public func addEntity(entity: Entity){
        self.entities[entity.id] = entity
        self.eventDidAddEntity(entity)
    }
    
    public func removeEntity(entity: Entity){
        self.entities[entity.id] = nil
        self.eventDidRemoveEntity(entity)
    }
    
    private func addQueuedEntities(){
        for entity in self.entitiesToAdd{
            self.entities[entity.id] = entity
            self.eventDidAddEntity(entity)
        }
        self.entitiesToAdd.removeAll()
    }
    
    private func removeQueuedEntities(){
        for entity in self.entitiesToRemove{
            self.entities[entity.id] = nil
            self.eventDidRemoveEntity(entity)
        }
        
        self.entitiesToRemove.removeAll()
    }
    
    public func createEntity() -> Entity{
        let entityID = self.idGenerator.generate()
        let entity = Entity(id: entityID)
        
        let mc = MoveComponent()
        let sc = SpeedComponent()
        let pc = PositionComponent()
        
        entity.addComponent(component: mc)
        entity.addComponent(component: sc)
        entity.addComponent(component: pc)
        
        self.addEntity(entity: entity)
        return entity
    }
    
    
}
