//
//  IDGenerator.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public protocol IDGenerator {

    func generate() -> EntityID
}
