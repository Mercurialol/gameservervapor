//
//  Component.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public typealias ComponentID = Int

public class Component{
    let id: ComponentID
    public var entity: Entity!
    
    init(id: ComponentID){
        self.id = id
    }
    
    func toJSON() -> (String? ,Any){
        return (nil, [:])
    }
}
