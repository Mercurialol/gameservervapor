//
//  Movement.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class Movement{
    // returns new position
    static public func move(currentPosition: Vector3, direction: Vector3, step: Double) -> Vector3{
        return currentPosition + (direction.normalized() * step)
    }
    
    static public func isValidPosition(entity: Entity, position: Point) -> Bool{
        return true
    }
}
