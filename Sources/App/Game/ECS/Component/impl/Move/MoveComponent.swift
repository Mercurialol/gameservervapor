//
//  MoveComponent.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class MoveComponent: Component{
    var direction: Vector3?
    var stopCondition: StopCondition
    
    init(){
        self.stopCondition = NoStopCondition.instance
        super.init(id: Constants.Component.TYPE_MOVE)
    }
    
    override func toJSON() -> (String? ,Any){
        if let dir = self.direction{
            return ("moveDirection", dir.toJSON())
        }else{
            return (nil, [:])
        }
    }
}

extension Entity{
    func moveComponent() -> MoveComponent?{
        return self.get(componentType: Constants.Component.TYPE_MOVE, type: MoveComponent.self)
    }
}
