//
//  GameObjectPositionProvider.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class GameObjectPositionProvider: PositionProvider{
    
    var entity: Entity!
    
    static func instantiate() -> GameObjectPositionProvider{
        return GameObjectPositionProvider()
    }
    
    private init(){}
    
    func providePosition() -> Point {
        return self.entity.positionComponent()!.position
    }
}

public extension Projectile{
    func target() -> Entity?{
        return ((self.moveComponent()?.stopCondition as? FollowTargetStopCondition)?.positionProvider as? GameObjectPositionProvider)?.entity
    }
}
