//
//  PositionProvider.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

protocol PositionProvider{
    func providePosition() -> Point
}
