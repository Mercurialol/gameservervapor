//
//  ZeroPointPositionProvider.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public class ZeroPointPositionProvider: PositionProvider{
    private static let fixedPoint = Point.zero
    public static let instance = ZeroPointPositionProvider()
    
    private init(){}
    
    func providePosition() -> Point {
        return ZeroPointPositionProvider.fixedPoint
    }
}
