//
//  FixedPointPositionProvider.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class FixedPointPositionProvider: PositionProvider{
    
    var point: Point
    
    init(point: Point){
        self.point = point
    }
    
    public func providePosition() -> Point {
        return self.point
    }
}
