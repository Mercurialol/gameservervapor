//
//  SpeedComponent.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class SpeedComponent: Component {
    var speed: Double
    
    init(){
        self.speed = 0
        super.init(id: Constants.Component.TYPE_SPEED)
    }
    
    override func toJSON() -> (String? ,Any){
        return ("speed", self.speed)
    }
}

public extension Entity{

    func setSpeed(speed: Double){
        self.get(componentType: Constants.Component.TYPE_SPEED, type: SpeedComponent.self)!.speed = speed
    }
}
