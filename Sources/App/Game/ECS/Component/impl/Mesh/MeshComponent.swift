//
//  MeshComponent.swift
//  App
//
//  Created by Nikola Markovic on 7/30/18.
//

import Foundation

public typealias MeshID = Int

public class MeshComponent: Component{
    
    let meshID: MeshID
    
    public init(meshID: MeshID){
        self.meshID = meshID
        super.init(id: C.Component.TYPE_MESH)
    }
}

public extension Entity{
    func meshComponent() -> MeshComponent?{
        return self.get(componentType: C.Component.TYPE_MESH, type: MeshComponent.self)
    }
}
