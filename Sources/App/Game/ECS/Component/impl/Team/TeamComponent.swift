//
//  TeamComponent.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/9/18.
//

import Foundation

public class TeamComponent: Component {
    let team: Team
    
    init(team: Team){
        self.team = team
        super.init(id: Constants.Component.TYPE_TEAM)
    }
    
    override func toJSON() -> (String? ,Any){
        return ("team", self.team.rawValue)
    }
}

extension Entity{
    func setTeam(team: Team){
        self.addComponent(component: TeamComponent(team: team))
    }
    
    func getTeam() -> Team?{
        return self.teamComponent()?.team
    }
    
    func teamComponent() -> TeamComponent?{
        return self.get(componentType: Constants.Component.TYPE_TEAM, type: TeamComponent.self)
    }
}
