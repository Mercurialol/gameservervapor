//
//  Team.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/9/18.
//

import Foundation

public enum Team: Int{
    case one = 1,
         two = 2
}
