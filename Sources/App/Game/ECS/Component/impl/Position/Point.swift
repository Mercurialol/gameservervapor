//
//  Point.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public typealias Vector3 = Point

public struct Point {
    var x: Double
    var y: Double
    var z: Double
    
    var length: Double{
        get{
            return sqrt( (self.x * self.x + self.y * self.y + self.z * self.z) )
        }
    }
    
    static var zero: Point{
        get{
            return Point(x: 0, y: 0, z: 0)
        }
    }
    
    public func distance(from other: Vector3) -> Double{
        let dx = self.x - other.x
        let dy = self.y - other.y
        let dz = self.z - other.z
        return sqrt(dx * dx + dy * dy + dz * dz)
    }
    
    mutating public func add(_ other: Vector3){
        self.x += other.x
        self.y += other.y
        self.z += other.z
    }
    
    mutating public func normalize(){
        let length = self.length
        self.x /= length
        self.y /= length
        self.z /= length
    }
    
    public func normalized() -> Point{
        var new = self
        new.normalize()
        return new
    }
    
    mutating public func setLength(length: Double){
        self.normalize()
        self = self * length
    }
}

extension Point{
    func toJSON() -> [String: Any]{
        return ["x": self.x, "y": self.y, "z": self.z]
    }
    
    func rawString() -> String{
        return "\(self.x);\(self.y);\(self.z)"
    }
}

public func -(lhs: Point, rhs: Point) -> Point{
    return Point(x: lhs.x - rhs.x, y: lhs.y - rhs.y, z: lhs.z - rhs.z)
}

public func +(lhs: Point, rhs: Point) -> Point{
    return Point(x: lhs.x + rhs.x, y: lhs.y + rhs.y, z: lhs.z + rhs.z)
}

public func *(lhs: Point, rhs: Double) -> Point{
    return Point(x: lhs.x * rhs, y: lhs.y * rhs, z: lhs.z * rhs)
}

