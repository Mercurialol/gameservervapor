//
//  PositionComponent.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class PositionComponent: Component {
    var position: Point
    
    init(){
        self.position = Point.zero
        super.init(id: Constants.Component.TYPE_POSITION)
    }
    
    override func toJSON() -> (String? ,Any){
        return ("position", self.position.toJSON())
    }
}

extension Entity{
    func positionComponent() -> PositionComponent? { return self.get(componentType: Constants.Component.TYPE_POSITION, type: PositionComponent.self)}
}
