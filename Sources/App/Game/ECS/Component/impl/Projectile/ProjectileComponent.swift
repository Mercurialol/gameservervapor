//
//  ProjectileComponent.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public class ProjectileComponent: Component{
    
    var target: Entity?
    var didReachTarget: Bool
    var reachTargetHandler: ((_ projectileComponent: ProjectileComponent, _ entity: Entity) -> Void)?
    
    init() {
        self.didReachTarget = false
        self.reachTargetHandler = nil
        super.init(id: Constants.Component.TYPE_PROJECTILE)
    }
    
    override func toJSON() -> (String?, Any) {
        return ("projectile", [String : Any]())
    }
}

public extension Entity{
    func projectileComponent() -> ProjectileComponent?{
        return self.get(componentType: Constants.Component.TYPE_PROJECTILE, type: ProjectileComponent.self)
    }
}
