//
//  VoidComponent.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class VoidComponent: Component{
    init() {
        super.init(id: Constants.Component.TYPE_VOID)
    }
}
