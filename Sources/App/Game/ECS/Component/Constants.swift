//
//  Constants.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

typealias C = Constants  

struct Constants{
    
    struct Time{
        static let NANO_SECONDS_PER_SECOND: UInt64 = 1000000000
        static let NANO_SECONDS_PER_SECOND_DOUBLE: Double = Double(NANO_SECONDS_PER_SECOND)
    }
    
    struct Component{
        static let TYPE_VOID = 0
        static let TYPE_SPEED = 1
        static let TYPE_POSITION = 2
        static let TYPE_MOVE = 3
        static let TYPE_TEAM = 4
        static let TYPE_PROJECTILE = 5
        static let TYPE_MESH = 6
    }
    
    struct System{
        static let MOVEMENT = 0
        static let PROJECTILE = 1
    }
    
    struct Math{
        static let objectReachDistance = 0.2 
    }
    
    struct Game{
        static let updateInterval: TimeInterval = 0.03
        static let minimumNumberOfPlayersToStartTheGame = 2
    }
    
    struct Command{
        static let MOVE = 1
    }
}
