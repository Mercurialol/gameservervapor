//
//  StopCondition.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public protocol StopCondition{
    func shouldStop(moveComponent: MoveComponent) -> Bool
}
