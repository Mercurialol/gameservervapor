//
//  NoStopCondition.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

class NoStopCondition: StopCondition{
    
    public static let instance = NoStopCondition()
    
    private init(){}
    
    func shouldStop(moveComponent: MoveComponent) -> Bool{
        return false
    }
}
