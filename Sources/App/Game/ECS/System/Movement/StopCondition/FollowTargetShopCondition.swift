//
//  TargetReachShopCondition.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public class FollowTargetStopCondition: StopCondition{
    
    var positionProvider: PositionProvider
    var pointComparator: PointComparator
    
    public static func instantiate() -> FollowTargetStopCondition{
        return FollowTargetStopCondition()
    }
    
    private init(pointComparator: @escaping PointComparator = defaultPointComparator){
        self.positionProvider = ZeroPointPositionProvider.instance
        self.pointComparator = pointComparator
    }

    public func shouldStop(moveComponent: MoveComponent) -> Bool {
        if let pos = moveComponent.entity.positionComponent(){
            return self.pointComparator(pos.position, self.positionProvider.providePosition())
        }else{
            return true
        }
    }
}
