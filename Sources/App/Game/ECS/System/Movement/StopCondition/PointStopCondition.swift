//
//  PointStopCondition.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public typealias PointComparator = (_ p1: Vector3, _ p2: Vector3) -> Bool

public var defaultPointComparator: PointComparator = { (p1, p2) -> Bool in
    return p1.distance(from: p2) <= Constants.Math.objectReachDistance
}

public class PointStopCondition: StopCondition, Poolable{
    
    var target: Vector3
    var pointComparator: PointComparator
    
    private static var objectPool: ObjectPool<PointStopCondition> = ObjectPool<PointStopCondition>(initializer: { () -> PointStopCondition in
        return PointStopCondition()
    }, count: 20)
    
    public static func instantiate() -> PointStopCondition{
        return PointStopCondition.objectPool.instantiate()
    }
    
    public func recycle(){
        PointStopCondition.objectPool.recycle(self)
    }
    
    private init(pointComparator: @escaping PointComparator = defaultPointComparator){
        self.pointComparator = pointComparator
        self.target = Vector3.zero
    }
    
    public func shouldStop(moveComponent: MoveComponent) -> Bool {
        if let pos = moveComponent.entity.positionComponent(){
            return self.pointComparator(pos.position, self.target)
        }else{
            return true
        }
    }
}
