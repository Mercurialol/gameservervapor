//
//  MovementSystem.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class MovementSystem: BaseSystem {

    override public func accepts(entity: Entity) -> Bool{
        return entity.has(componentType: Constants.Component.TYPE_POSITION) &&
                entity.has(componentType: Constants.Component.TYPE_SPEED) &&
                entity.has(componentType: Constants.Component.TYPE_MOVE)
    }
    
    public func applyMoveCommand(command: MoveCommand, context: GameContext){
        if let entity = context.entityManager.entityWithID(id: command.entityID){
            if self.isValidPosition(entity: entity, position: command.position){
                
                // update the position
                entity.get(componentType: Constants.Component.TYPE_MOVE, type: MoveComponent.self)!.direction = command.direction
                entity.get(componentType: Constants.Component.TYPE_POSITION, type: PositionComponent.self)!.position = command.position
                
                // set dirty
                context.broadcaster.appendBroadcastCommand(command: command)
            }
        }
    }
    
    public func isValidPosition(entity: Entity, position: Point) -> Bool{
        return true
    }
    
    override public func update(deltaTime: TimeInterval){
        for entity in self.entities{
            let mc = entity.get(componentType: Constants.Component.TYPE_MOVE, type: MoveComponent.self)!
        
            if let direction = mc.direction, !mc.stopCondition.shouldStop(moveComponent: mc){
                let sc = entity.get(componentType: Constants.Component.TYPE_SPEED, type: SpeedComponent.self)!
                let pc = entity.get(componentType: Constants.Component.TYPE_POSITION, type: PositionComponent.self)!
                
                let step = sc.speed * deltaTime
                pc.position = Movement.move(currentPosition: pc.position, direction: direction, step: step)
            }
            
        }
    }

}
