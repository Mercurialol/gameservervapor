//
//  System.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public protocol System: class{
    
    var entities: [Entity] { get set }
    
    func accepts(entity: Entity) -> Bool
    func update(deltaTime: TimeInterval)
    
}
