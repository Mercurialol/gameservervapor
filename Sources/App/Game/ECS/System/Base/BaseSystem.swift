//
//  BaseSystem.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class BaseSystem: System{
    public var entities: [Entity]
    
    init() {
        self.entities = []
    }
    
    public func accepts(entity: Entity) -> Bool{
        fatalError("accepts not implemented in \(self)")
    }
    
    public func update(deltaTime: TimeInterval){
        fatalError("update not implemented in \(self)")
    }
    
}
