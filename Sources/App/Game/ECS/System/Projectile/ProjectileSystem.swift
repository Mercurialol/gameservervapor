//
//  ProjectileSystem.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public class ProjectileSystem: BaseSystem {

    override public func accepts(entity: Entity) -> Bool {
        return entity.has(componentType: Constants.Component.TYPE_PROJECTILE) &&
                entity.has(componentType: Constants.Component.TYPE_MOVE)
    }
    
    override public func update(deltaTime: TimeInterval){
        for entity in self.entities{
            let pc = entity.get(componentType: Constants.Component.TYPE_PROJECTILE, type: ProjectileComponent.self)!
            
            if pc.didReachTarget{
                continue
            }
            
            let mc = entity.get(componentType: Constants.Component.TYPE_MOVE, type: MoveComponent.self)!
            
            if mc.stopCondition.shouldStop(moveComponent: mc){
                // reached target
                pc.reachTargetHandler?(pc, entity)
                pc.didReachTarget = true
            }
        }
    }
}
