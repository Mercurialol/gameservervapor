//
//  Random.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/12/18.
//

import Foundation

extension Int{
    static func random(min: Int = 0, max: Int = Int.max) -> Int{
        #if os(Linux)
            return Glibc.random() % max
        #else
        
            return Int(arc4random())
        #endif
    }
}
