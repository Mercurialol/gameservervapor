//
//  Time.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class Time {
    private(set) var time: TimeInterval
    private(set) var deltaTime: TimeInterval
    
    init(){
        self.time = Date().timeIntervalSince1970
        self.deltaTime = 0
    }
    
    public func update(){
        let currentTimeInterval = Date().timeIntervalSince1970
        self.deltaTime = currentTimeInterval - self.time
        self.time = currentTimeInterval
    }
}
