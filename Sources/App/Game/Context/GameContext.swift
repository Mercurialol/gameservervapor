//
//  GameContext.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation
import WebSocket

public class GameContext {
    private(set) var systems: [System]
    
    public let commandManager: CommandManager
    public let entityManager: EntityManager
    public let playerManager: PlayerManager
    public let broadcaster: Broadcaster
    
    let time: Time
    
    var currentState: GameState
    
    var running: Bool
    
    init(){
        self.systems = []
        self.systems.append(MovementSystem())
        self.systems.append(ProjectileSystem())
        
        self.commandManager = CommandManager()
        self.entityManager = EntityManager()
        self.playerManager = PlayerManager()
        self.broadcaster = Broadcaster(playerManager: playerManager)
    
        self.time = Time()
        
        self.running = true
        
        self.currentState = self.stateRunning
        
        self.entityManager.eventDidAddEntity = { entity in
            for system in self.systems{
                if system.accepts(entity: entity){
                    system.entities.append(entity)
                }
            }
        }
        
        self.entityManager.eventDidRemoveEntity = { entity in
            for system in self.systems{
                if system.accepts(entity: entity){
                    if let idx = system.entities.index(where: { (e) -> Bool in return e.id == entity.id }) {
                        system.entities.remove(at: idx)
                    }
                }
            }
        }
    }
    
    public func broadcastState(){
        let jsonData = self.generateStateData()
        
        self.broadcaster.broadcastData(data: jsonData)
    }
    
    public func generateStateData() -> Data{
        let gameDescription = self.captureGameState()
        let jsonDict = gameDescription.toJSON()
        print("Generating \(jsonDict).")
        return try! JSONSerialization.data(withJSONObject: jsonDict, options: [])
    }
    
    public func update(deltaTime: Double){
        if !running { return }
        
        for system in self.systems{
            system.update(deltaTime: deltaTime)
        }
        
        self.commandManager.executeScheduledCommands(context: self)
        
        self.entityManager.update(deltaTime: deltaTime)
        
        self.broadcaster.broadcastMessages()
    }
    
    public func setState(state: GameState){
        print("set state \(state)")
        if state === self.currentState { return }
        self.currentState.willSwitchAwayFromThisState(context: self)
        self.currentState = state
        self.currentState.didSwitchToThisState(context: self)
    }
    
    public func playerConnected(socket: WebSocket, entityManager: EntityManager) -> Player{
        let player = self.playerManager.playerConnected(socket: socket, entityManager: self.entityManager)
        self.currentState.playerConnected(socket: socket, context: self)
//        if self.currentState === self.stateRunning || self.currentState === self.stateStartingGame{
//            self.sendGameState(to: socket)
//        }else if self.playerManager.hasEnoughPlayersToStartTheGame(){
//            self.setState(state: self.stateStartingGame)
//        }
        
        return player
    }
    
    let startingPositionTeam1: Vector3 = Vector3(x: -172.7, y: 0.7, z: 48.2)
    let startingPositionTeam2: Vector3 = Vector3(x: -147.2, y: -0.526, z: 48.08)

    public func sendGameState(to socket: WebSocket){
        print("sending game state... ")
        let state = self.captureGameState()
        socket.send(state.toJSONData())
    }
    
    public func captureGameState() -> GameDescription{
        print("Capture game state")
        for (idx,player) in self.playerManager.players.enumerated(){
            if idx % 2 == 0{
                player.entity.setTeam(team: Team.one)
                player.entity.positionComponent()!.position = self.startingPositionTeam1
            }else{
                player.entity.setTeam(team: Team.two)
                player.entity.positionComponent()!.position = self.startingPositionTeam2
            }
        }

        let gameDescription = GameDescription(context: self)
        return gameDescription
    }
    
    let stateWaitingForPlayers = WaitingForPlayersGameState()
    let stateStartingGame = StartingGameGameState()
    let stateRunning = RunningGameState()
    
}
