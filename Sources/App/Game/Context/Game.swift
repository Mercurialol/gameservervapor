//
//  Game.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation
import Dispatch
import WebSocket

var _game: Game!

public func startGame() -> Game{
    _game = Game()
    return _game
}

public class Game{
    public var context: GameContext
    var timer: Timer!
    var updateInterval: TimeInterval
    let queue: DispatchQueue
    
    public init(){
        self.context = GameContext()
        self.updateInterval = Constants.Game.updateInterval
        
        self.queue = DispatchQueue(label: "game")
        self.scheduleUpdate()
    }
    
    var lastUpdate: TimeInterval = 0
    
    func scheduleUpdate(){
        let updateDuration = Date().timeIntervalSince1970 - lastUpdate
        var scheduleIn: TimeInterval
        if(updateDuration > self.updateInterval){
            scheduleIn = 0
        }else{
            scheduleIn = self.updateInterval - updateDuration
        }
        
//        print("Schedule in \(scheduleIn)")
        
        let dispatchTime = DispatchTime(uptimeNanoseconds: DispatchTime.now().uptimeNanoseconds + UInt64(Constants.Time.NANO_SECONDS_PER_SECOND_DOUBLE * scheduleIn))
        
        self.queue.asyncAfter(deadline: dispatchTime, execute: { [weak self] in
            self?.update()
        })
    }
    
    public func handleInput(webSocket: WebSocket, input: String){
        print("State handling input is \(self.context.currentState)")
        self.context.currentState.handleInput(input: input, socket: webSocket, context: self.context)
    }
    
    @objc
    public func update(){
        self.context.time.update()
        self.lastUpdate = self.context.time.time
        self.context.update(deltaTime: self.context.time.deltaTime)
        
        self.scheduleUpdate()
    }
 
}
