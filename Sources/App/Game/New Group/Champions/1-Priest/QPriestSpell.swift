//
//  QPriestSpell.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/27/18.
//

import Foundation

class QPriestSpell: DirectionalSpellCommand {

    let rootDuration: Double
    let range: Double
    
    required public init(parameters: [String], rawString: String) {
        self.rootDuration = GameConfig.instance.champion(name: GCC.Champion.priest).spellQ().rootDuration()
        self.range = GameConfig.instance.champion(name: GCC.Champion.priest).spellQ().range()
        super.init(parameters: parameters, rawString: rawString)
    }
    
    override func execute(context: GameContext) {
        if let parentEntity = context.entityManager.entityWithID(id: self.entityID){
            let projectile = Projectile.instantiate()
            projectile.setDirectional(position: parentEntity.positionComponent()!.position, direction: self.direction, range: self.range)
            projectile.projectileComponent()!.reachTargetHandler = { (pc, entity) in
                context.entityManager.removeEntity(entity: pc.entity)
            }
            context.entityManager.addEntity(entity: projectile)
        }
    }
}
