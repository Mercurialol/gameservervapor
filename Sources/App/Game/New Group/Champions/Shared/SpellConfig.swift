//
//  SpellConfig.swift
//  App
//
//  Created by Nikola Markovic on 7/29/18.
//

import Foundation

public class SpellConfig{
    let dictionary: [String: Any]
    
    init(dictionary: [String: Any]){
        self.dictionary = dictionary
    }
}

public extension SpellConfig{
    func rootDuration() -> Double{
        return self.dictionary[GCC.Key.rootDuration] as! Double
    }
    
    func damage() -> Double{
        return Double(self.dictionary[GCC.Key.damage] as! Int)
    }
    
    func range() -> Double{
        return Double(self.dictionary[GCC.Key.range] as! Int)
    }
}
