//
//  ChampionConfig.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/28/18.
//

import Foundation

public class ChampionConfig: NSObject {

    let dictionary: [String: Any]
    
    let spellConfigs: [SpellConfig]
    
    init(dictionary: [String: Any]){
        self.dictionary = dictionary
        
        self.spellConfigs = (self.dictionary[GCC.Shared.spells] as! [[String: Any]]).map({ dict -> SpellConfig in
            return SpellConfig(dictionary: dict)
        })
    }
    
    
}

public extension ChampionConfig{
    func spellQ() -> SpellConfig{
        return self.spellConfigs[0]
    }
    
    func spellW() -> SpellConfig{
        return self.spellConfigs[1]
    }
    
    func spellE() -> SpellConfig{
        return self.spellConfigs[2]
    }
    
    func spellR() -> SpellConfig{
        return self.spellConfigs[3]
    }
}
