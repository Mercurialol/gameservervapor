//
//  Config.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/28/18.
//

import Foundation

public class GameConfig{

    public static let instance = GameConfig()
    
    private let dictionary: [String: Any]
    private var champions: [String: ChampionConfig]
    
    private init(){
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "config", ofType: "json")!)
        let data = try! Data(contentsOf: url)
        self.dictionary = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
        
        self.champions = [:]
        for d in (dictionary["champions"] as! [[String: Any]]){
            let key: String = d[GCC.Key.name] as! String
            champions[key] = ChampionConfig(dictionary: d)
        }
    }
    
    func champion(name: String) -> ChampionConfig{
        return self.champions[name]!
    }
}

// Game config constants
public struct GCC{
    struct Shared{
        static let spells = "spells"
    }
    
    struct Champion{
        static let priest = "priest"
    }
    
    struct Key{
        static let name = "name"
        static let minDamage = "minDamage"
        static let maxDamage = "maxDamage"
        static let damage = "damage"
        static let range = "range"
        static let rootDuration = "rootDuration"
        static let duration = "duration"
        
    }
}
