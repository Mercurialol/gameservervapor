//
//  RunningGameState.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/8/18.
//

import Foundation
import WebSocket

public class RunningGameState: GameState {
    
    let parser: NetworkDataParser
    
    init(){
         self.parser = DefaultNetworkDataParser()
    }
    
    public func didSwitchToThisState(context: GameContext){
        context.running = true
    }
    
    public func handleInput(input: String, socket: WebSocket, context: GameContext){
        print("RunningGameState handleInput received \(input)")
        let commands = self.parser.parse(string: input, commandManager: context.commandManager)
        context.commandManager.appendCommands(commands: commands)
    }
    
    public func willSwitchAwayFromThisState(context: GameContext){
        
    }
    
    public func playerConnected(socket: WebSocket, context: GameContext){
        context.sendGameState(to: socket)
    }
    
}
