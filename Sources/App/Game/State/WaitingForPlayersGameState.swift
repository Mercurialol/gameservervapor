//
//  WaitingForPlayersGameState.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/8/18.
//

import Foundation
import WebSocket

public class WaitingForPlayersGameState: GameState {

    public func didSwitchToThisState(context: GameContext){}
    
    public func handleInput(input: String, socket: WebSocket, context: GameContext){
        // ignore any input while players are connecting
    }
    
    public func willSwitchAwayFromThisState(context: GameContext){}
    
}
