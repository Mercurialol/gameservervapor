//
//  StartingGameGameState.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/8/18.
//

import Foundation
import WebSocket

public class StartingGameGameState: GameState {
    
    let startingPositionTeam1: Vector3
    let startingPositionTeam2: Vector3
    
    var dataBroadcasted: Bool
    
    var readyPlayers: [PlayerID: Player]
    
    init(){
        self.readyPlayers = [:]
        self.startingPositionTeam1 = Vector3(x: -172.7, y: 0.7, z: 48.2)
        self.startingPositionTeam2 = Vector3(x: -147.2, y: -0.526, z: 48.08)
        self.dataBroadcasted = false
    }
    
    public func didSwitchToThisState(context: GameContext){
        print("Starting game...")
        self.dataBroadcasted = false
        self.readyPlayers.removeAll()

        context.broadcastState()
        
        self.dataBroadcasted = true
        
        /*   WAIT FOR CONFIRMATION THAT EVERYONE IS READY TO START THE GAME  */
    }
    
    public func handleInput(input: String, socket: WebSocket, context: GameContext){
        if dataBroadcasted{
            // data broadcasted, let's see who's ready
            
            if input == "ready"{
                if let player = context.playerManager.playerWithSocket(socket: socket){
                    self.readyPlayers[player.id] = player
                }
            }
            
            for player in context.playerManager.players{
                if readyPlayers[player.id] == nil{
                    // found a player that isnt ready
                    return
                }
            }
            
            context.broadcaster.broadcastMessage(message: "started")
            
            // everyone is ready
            context.setState(state: context.stateRunning)
        }
    }
    
    public func willSwitchAwayFromThisState(context: GameContext){
        self.dataBroadcasted = false
        self.readyPlayers.removeAll()
    }
}
