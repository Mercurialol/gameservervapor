//
//  GameState.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/8/18.
//

import Foundation
import WebSocket

public protocol GameState: class{
    
    func didSwitchToThisState(context: GameContext)
    func handleInput(input: String, socket: WebSocket, context: GameContext)
    func willSwitchAwayFromThisState(context: GameContext)
    
    func playerConnected(socket: WebSocket, context: GameContext)
    func playerDisonnected(socket: WebSocket, context: GameContext)
    
}

public extension GameState{
    
    func playerConnected(socket: WebSocket, context: GameContext){}
    func playerDisonnected(socket: WebSocket, context: GameContext){}
}
