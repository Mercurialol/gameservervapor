//
//  DirectionalSpellCommand.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/27/18.
//

import Foundation

class DirectionalSpellCommand: CastSpellCommand {

    let direction: Vector3
    
    required public init(parameters: [String], rawString: String){
        self.direction = ParseHelper.pointFromParameters(parameters: parameters, startIndex: 6)
        super.init(parameters: parameters, rawString: rawString)
        
    }
}
