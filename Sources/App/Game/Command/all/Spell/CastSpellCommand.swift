//
//  CastSpellCommand.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/27/18.
//

import Foundation

class CastSpellCommand: Command {

    let position: Vector3
    let spellIndex: Int
    let entityID: EntityID
    let receivedPacketAt: TimeInterval
    let ping: TimeInterval
    
    let rawString: String
    
    required public init(parameters: [String], rawString: String){
        self.entityID = EntityID(parameters[1])!
        self.position = ParseHelper.pointFromParameters(parameters: parameters, startIndex: 2)
        self.spellIndex = Int(parameters[2])!
//        self.direction = ParseHelper.pointFromParameters(parameters: parameters, startIndex: 5)
        self.receivedPacketAt = 0
        self.ping = 0
        
        self.rawString = rawString
    }
    
    public func execute(context: GameContext) {}
}
