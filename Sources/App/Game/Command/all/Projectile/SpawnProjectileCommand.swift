//
//  SpawnProjectileCommand.swift
//  App
//
//  Created by Nikola Markovic on 7/30/18.
//

import Foundation

public class SpawnProjectileCommand: Command{
    
    var projectile: Projectile
    public let rawString: String
    
    public init(projectile: Projectile){
        self.projectile = projectile
        
        let id = self.projectile.directional ? 3 : 4
        let positionStr = projectile.parent.positionComponent()!.position.rawString()
        var lastComponent: String!
        if(self.projectile.directional){
            // directional
            lastComponent = projectile.moveComponent()!.direction!.rawString()
        }else{
            // targeted
            lastComponent = "\(projectile.target()!.id)"
        }
        
        self.rawString = "\(id);\(projectile.parent.id);\(projectile.meshComponent()!.meshID);\(positionStr);\(lastComponent!)"
    }
    
    public func execute(context: GameContext) {
        context.entityManager.addEntity(entity: self.projectile)
        context.broadcaster.appendBroadcastCommand(command: self)
    }
}
