//
//  MoveCommand.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class MoveCommand: Command {

    public let rawString: String
    
    public let position: Vector3
    public let direction: Vector3
    public let entityID: EntityID
    
    required public init(parameters: [String], rawString: String){
        self.entityID = EntityID(parameters[1])!
        self.position = ParseHelper.pointFromParameters(parameters: parameters, startIndex: 2)
        self.direction = ParseHelper.pointFromParameters(parameters: parameters, startIndex: 5)
        
        self.rawString = rawString
    }
    
    public func execute(context: GameContext) {
        let movementSystem = context.systems[C.System.MOVEMENT] as! MovementSystem
        movementSystem.applyMoveCommand(command: self, context: context)
    }
}
