//
//  Command.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public protocol Command{
    
    var rawString: String { get }
    
    func execute(context: GameContext)
}
