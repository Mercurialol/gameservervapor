//
//  NetworkCommand.swift
//  App
//
//  Created by Nikola Markovic on 7/30/18.
//

import Foundation

public class NetworkCommand: Command{
    public let receivedPacketAt: TimeInterval
    public let ping: TimeInterval
    
    public let rawString: String
    
    public init(parameters: [String], rawString: String) {
        self.receivedPacketAt = 0
        self.ping = 0
        
        self.rawString = rawString
    }
    
    public func execute(context: GameContext) {}
}
