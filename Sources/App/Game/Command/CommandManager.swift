//
//  CommandManager.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class CommandManager {
    var commandQueue: [Command]
    
    init(){
        self.commandQueue = []
    }
    
    public func executeScheduledCommands(context: GameContext){
        for command in self.commandQueue{
            command.execute(context: context)
        }
        
        self.commandQueue.removeAll(keepingCapacity: false)
    }
    
    public func getCommand(commandType: CommandType, parameters: [String], rawString: String) -> Command{
        switch commandType {
        case .move:
            return MoveCommand(parameters: parameters, rawString: rawString)
        }
    }
    
    public func appendCommands(commands: [Command]){
        self.commandQueue.append(contentsOf: commands)
    }
}
