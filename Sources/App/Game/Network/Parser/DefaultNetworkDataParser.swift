//
//  DefaultNetworkDataParser.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class DefaultNetworkDataParser: NetworkDataParser{

    public init(){}
    
    public func parse(string: String, commandManager: CommandManager) -> [Command]{
        var commands = [Command]()
        for string in string.split(separator: "+").map({ (substring) -> String in return String(substring) }){
            let items = string.split(separator: ";").map({ (substring) -> String in return String(substring) })
            
            if items.count == 0 { continue }
            
            if let commandID = Int(items[0]),
                let commandType = CommandType(rawValue: commandID){
                commands.append(commandManager.getCommand(commandType: commandType, parameters: items, rawString: string))
            }
        }
        return commands
    }
    
}
