//
//  ParseHelper.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public class ParseHelper{

    static public func pointFromParameters(parameters: [String], startIndex: Int) -> Point{
        let x = Double(parameters[startIndex])!
        let y = Double(parameters[startIndex+1])!
        let z = Double(parameters[startIndex+2])!
        return Point(x: x, y: y, z: z)
    }
}
