//
//  NetworkDataParser.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation

public protocol NetworkDataParser{
    func parse(string: String, commandManager: CommandManager) -> [Command]
}
