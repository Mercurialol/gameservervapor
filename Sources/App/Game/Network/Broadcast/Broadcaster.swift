//
//  Broadcaster.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation
import WebSocket

public class Broadcaster {

    private var queue: [Command]
    let playerManager: PlayerManager
    
    init(playerManager: PlayerManager) {
        self.queue = []
        self.playerManager = playerManager
    }
    
    public func appendBroadcastCommand(command: Command){
        print("Appending to queue \(command.rawString)")
        self.queue.append(command)
    }
    
    public func getString() -> String{
        return queue.reduce("", { (res: String, next: Command) -> String in
            return res.count == 0 ? next.rawString : "\(res)+\(next.rawString)"
        })
    }
    
    public func broadcastMessages(){
        if self.queue.count > 0{
            let message = self.getString()
            self.broadcastMessage(message: message)
            self.clear()
        }
    }
    
    public func broadcastMessage(message: String){
        print("Broadcasting \(message) to \(self.playerManager.players.count) players")
        for player in self.playerManager.players{
            player.socket.send(message)
        }
    }
    
    public func broadcastData(data: Data){
        print("Broadcasting data to \(self.playerManager.players.count) players")
        for player in self.playerManager.players{
            self.send(data: data, to: player)
        }
    }
    
    public func send(data: Data, to player: Player){
        player.socket.send(data)
    }
    
    public func clear(){
        print("Clear queue \(self.queue.count)")
        self.queue.removeAll()
        print("queue now has \(self.queue.count)")
    }
}
