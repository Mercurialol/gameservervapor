//
//  PlayerManager.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import WebSocket

public class PlayerManager{
    
    var players: [Player]
    var playerIDGenerator: IDGenerator
    
    init() {
        self.players = []
        self.playerIDGenerator = RandomIDGenerator()
    }
    
    public func playerConnected(socket: WebSocket, entityManager: EntityManager) -> Player{
        // create entity
        let entity = entityManager.createEntity()
        let player = Player(id:self.playerIDGenerator.generate(), entity: entity, socket: socket)
        self.players.append(player)
        print("Appending player, count is now \(self.players.count)")
        return player
    }
    
    public func playerWithSocket(socket: WebSocket) -> Player?{
        for player in self.players{
            if player.socket === socket{
                return player
            }
        }
        
        return nil
    }
    
    public func playerDisconnected(player: Player){
        print("Player \(player.id) disconnected")
        self.removePlayer(playerID: player.id)
    }
    
    public func hasEnoughPlayersToStartTheGame() -> Bool{
        return self.players.count >= Constants.Game.minimumNumberOfPlayersToStartTheGame 
    }
    
    private func removePlayer(playerID: PlayerID){
        if let idx = self.players.index( where: { (player) -> Bool in return player.id == playerID } ){
            self.players.remove(at: idx)
        }
        
        print("Player count is now \(self.players.count)")
    }
}
