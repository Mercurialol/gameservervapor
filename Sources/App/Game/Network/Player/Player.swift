//
//  Player.swift
//  SRVPackageDescription
//
//  Created by Nikola Markovic on 7/7/18.
//

import Foundation
import WebSocket

public typealias PlayerID = Int

public class Player {
    public let id: PlayerID
    public let entity: Entity
    public let socket: WebSocket

    init(id: PlayerID, entity: Entity, socket: WebSocket){
        self.id = id
        self.entity = entity
        self.socket = socket
    }
}
