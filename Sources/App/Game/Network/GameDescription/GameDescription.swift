//
//  GameDescription.swift
//  App
//
//  Created by Nikola Markovic on 7/9/18.
//

import Foundation

public class GameDescription{
    
    let context: GameContext
    
    init(context: GameContext){
        self.context = context
    }
    
    func toJSON() -> [String: Any]{
        var json: [String: Any] = [:]
        var entities: [[String: Any]] = []
        for entity in context.entityManager.entities.values{
            entities.append(entity.toJSON())
        }
        
        json["entities"] = entities
        return json
    }
    
    func toJSONData() -> Data{
        let jsonDict = self.toJSON()
        let jsonData = try! JSONSerialization.data(withJSONObject: jsonDict, options: [])
        return jsonData
    }
}

