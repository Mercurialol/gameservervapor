import App
import WebSocket

//try app(.detect()).run()

let game: Game = Game()

let socketUpgrader = HTTPServer.webSocketUpgrader(shouldUpgrade: { (req) -> (HTTPHeaders?) in
    
//    if req.url.path == "/deny"{
//        return nil
//    }
    
    return [:]
}) { (socket, req) in
    let player = game.context.playerConnected(socket: socket, entityManager: game.context.entityManager)
    socket.send("id:\(player.entity.id)")
    
    socket.onText { ws, string in
        print("Received \(string)")
        game.handleInput(webSocket: ws, input: string)
    }
    
    socket.onError({ (socket, err) in
        print("socket on err \(err.localizedDescription)")
    })
    
    socket.onCloseCode({ (errorCode) in
        print("socket on close \(errorCode)")
        game.context.playerManager.playerDisconnected(player: player)
    })
}

class MyResponder: HTTPServerResponder{
    func respond(to request: HTTPRequest, on worker: Worker) -> Future<HTTPResponse>{
        let promise = worker.eventLoop.newPromise(HTTPResponse.self)
        return promise.futureResult
    }
}

let eventGroupLoop = MultiThreadedEventLoopGroup(numberOfThreads: 1)


let server = try HTTPServer.start(hostname: "127.0.0.1", port: 8080, responder: MyResponder(), maxBodySize: 4096, backlog: 1024, reuseAddress: true, tcpNoDelay: true, upgraders: [socketUpgrader], on: eventGroupLoop) { (error) in
    print("Error occurred \(error.localizedDescription)")
}.wait()

try server.onClose.wait()
